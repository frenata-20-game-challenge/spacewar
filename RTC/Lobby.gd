class_name RTCLobby
extends Node

@export var PORT = 7000
@export var DEFAULT_SERVER_IP = "127.0.0.1" # IPv4 localhost
@export var MAX_CONNECTIONS = 20
@export var STUN_SERVER = "stun:stun.l.google.com:19302"

var client = preload("res://RTC/client.tscn")
var server = preload("res://RTC/server.tscn")

var node

func _ready():
	if DisplayServer.get_name() == "headless":
		print('make a server')
		add_child(server.instantiate().new(PORT, DEFAULT_SERVER_IP, MAX_CONNECTIONS))
	else:
		node = client.instantiate().new(PORT, DEFAULT_SERVER_IP, MAX_CONNECTIONS, STUN_SERVER)
		add_child(node)

func has_connection():
	return node != null and node.ready()

func channel():
	if has_connection():
		queue_free()
		return RTCChannel.new(node)
	

class RTCChannel:
	var channel
	var id
	
	func _init(node):
		channel = node.channel
		id = node.rtc.get_unique_id()
	
	func send(message):
		if channel != null and channel.get_ready_state() == WebRTCDataChannel.STATE_OPEN:
			channel.put_packet(message.to_utf8_buffer())
			
	func recv(): #-> Array[String]:
		var messages = []
		while channel != null and channel.get_available_packet_count() > 0:
			messages.push_back(channel.get_packet().get_string_from_utf8())
		return messages
