extends Area2D

@export var translation: Vector2

func _on_body_entered(body):
	if body.has_method("set_translation"):
		body.set_translation(translation)
