extends Node2D

var game = preload("res://game.tscn")
var lobby_scene = preload("res://RTC/lobby.tscn")
var lobby
var channel
var game_running = false

func _process(delta):
	if channel != null:
		if not game_running:
			channel.send(JSON.stringify("ready"))
			for raw in channel.recv():
				var msg = JSON.parse_string(raw)
				if typeof(msg) == TYPE_DICTIONARY:
					pass
				elif msg == "ready":
					start()
	elif has_node("Lobby") and get_node("Lobby").has_connection():
		# NOTE: this is important -- don't attempt to grab a pointer to the channel until the Lobby
		# has a valid connection ready
		channel = get_node("Lobby").channel() # NOTE: Lobby frees itself after returning the channel
		
func start():
	game_running = true
	toast("")
	var g = game.instantiate().new(channel)
	g.game_over.connect(_on_game_over)
	add_child(g)

func _on_game_over(player):
	game_running = false
	toast(player + " wins!!")

func _ready():
	toast("Waiting for another player to connect...")

func toast(message):
	%Toast.display(message)


func _on_ip_text_submitted(new_text):
	%IP.visible = false
	lobby = lobby_scene.instantiate()
	lobby.DEFAULT_SERVER_IP = new_text
	add_child(lobby)
