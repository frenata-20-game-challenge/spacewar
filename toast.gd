extends Label

func _on_toast_timeout():
	text = ""
	visible = false

func display(message):
	visible = true
	text = message
	%ToastTimer.start()
