extends RigidBody2D

var trajectory
var translation = Vector2.ZERO

func _ready():
	apply_central_impulse(trajectory * .6)

func new(trajectory, pos, rot):
	self.trajectory = trajectory
	self.position = pos
	self.rotation = rot

func set_translation(translation):
	self.translation = translation
	
func _integrate_forces(state):
	if translation != Vector2.ZERO:
		state.transform = Transform2D(transform.get_rotation(), transform.get_origin() + translation)
		translation = Vector2.ZERO

func _on_timeout():
	queue_free()

func _on_collision(body):
	if body.has_method("explode"):
		#print(self, " hit a ship")
		body.explode()
	elif body.is_in_group("planetary"):
		#print(self, " hit a planet")
		queue_free()
