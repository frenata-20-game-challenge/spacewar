extends InputController

@export var thrust_act: String = "thrust"
@export var shoot_act: String = "shoot"
@export var left_act: String = "left"
@export var right_act: String = "right"

var player

signal pressed(act, player)
signal just_pressed(act, player)

var state = {
			thrust_act: false,
			left_act: false,
			right_act: false,
			shoot_act: false
			}
			
func is_action_pressed(act):
	return state[act]

func is_action_just_pressed(act):
	return state[act]

func _process(delta):
	for act in [left_act, right_act, thrust_act]:
		if Input.is_action_pressed(act):
			pressed.emit(act, player)
			state[act] = true
		else:
			state[act] = false
	
	if Input.is_action_just_pressed(shoot_act):
		pressed.emit(shoot_act, player)
		state[shoot_act] = true
	else:
		state[shoot_act] = false
