extends Node2D

signal game_over(player)
var channel

var local = preload("res://local_input.tscn")
var remote = preload("res://remote_input.tscn")
var identified = false

func _on_player_death(player):
	#print(player, " died")
	pass

func _process(_delta):
	if not identified:
		print(str(channel.id) + " sends id")
		channel.send(JSON.stringify({"id": channel.id, "identified": identified}))
	var ships = get_tree().get_nodes_in_group("ships")
	var winner
	if len(ships) == 1:
		winner = ships[0].player
	elif len(ships) == 0:
		winner = "No-one"
	
	if winner != null:
		channel.send(JSON.stringify({"winner": winner}))
		handle_game_over(winner)

	
func handle_game_over(winner):
	print("HANDLE GAME OVER: ", winner)
	game_over.emit(winner)
	queue_free()


func new(rtc_channel):
	channel = rtc_channel
	return self


func _on_local_input_pressed(act, player):
	channel.send(JSON.stringify({"action": act, "type": "pressed", "player": player}))

func _on_local_input_just_pressed(act, player):
	channel.send(JSON.stringify({"action": act, "type": "just_pressed", "player": player}))

func _physics_process(_delta):
	var msgs = channel.recv()
	for raw in msgs:
		var msg = JSON.parse_string(raw)
		if typeof(msg) == TYPE_STRING:
			pass
		elif msg.get("action"):
			handle_action(msg)
		elif msg.get("id"):
			handle_identity(msg)
		elif msg.get("winner"):
			handle_game_over(msg.winner)
		
func handle_action(msg):
	get_node(msg.player).input.set_pressed(msg.action)

func handle_identity(msg):
	# Send ID back proactively
	if msg.identified == false:
		channel.send(JSON.stringify({"id": channel.id, "identified": identified}))
	if identified: # handling identity should be idempotent
		print(str(channel.id) + " passes on handling identity")
		return
		
	identified = true
	# NOTE: always make the lower ID the Red player
	var me
	var them
	if msg.id < channel.id:
		me = "%Blue"
		them = "%Red"
	else:
		me = "%Red"
		them = "%Blue"
		
	print("I am " + me + ". They are " + them + ".")
		
	var local_input = local.instantiate()
	get_node(me).input = local_input
	local_input.player = get_node(me).player
	local_input.pressed.connect(_on_local_input_pressed)
	local_input.just_pressed.connect(_on_local_input_just_pressed)
	get_node(me).add_child(local_input)

	var remote_input = remote.instantiate()
	get_node(them).input = remote_input
	remote_input.player = get_node(them).player
	get_node(them).add_child(remote_input)
