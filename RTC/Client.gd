extends Node

var PORT
var DEFAULT_SERVER_IP
var MAX_CONNECTIONS
var STUN_SERVER

var channel : WebRTCDataChannel

var rtc: WebRTCMultiplayerPeer = WebRTCMultiplayerPeer.new()
var rpc_state = null

func new(PORT, DEFAULT_SERVER_IP, MAX_CONNECTIONS, STUN_SERVER):
	self.PORT = PORT
	self.DEFAULT_SERVER_IP = DEFAULT_SERVER_IP
	self.MAX_CONNECTIONS = MAX_CONNECTIONS
	self.STUN_SERVER = STUN_SERVER
	return self

func _ready():
	print("I am a client")
	multiplayer.peer_connected.connect(_on_player_connected)
	multiplayer.peer_disconnected.connect(_on_player_disconnected)
	multiplayer.connected_to_server.connect(_on_connected_ok)
	multiplayer.connection_failed.connect(_on_connected_fail)
	multiplayer.server_disconnected.connect(_on_server_disconnected)
	join_game()

func _disconnect_signal_server():
	multiplayer.peer_connected.disconnect(_on_player_connected)
	multiplayer.peer_disconnected.disconnect(_on_player_disconnected)
	multiplayer.connected_to_server.disconnect(_on_connected_ok)
	multiplayer.connection_failed.disconnect(_on_connected_fail)
	multiplayer.server_disconnected.disconnect(_on_server_disconnected)



func rand_name():
	var name = ""
	for _i in range(5):
		var choice = randi() % 26
		name += "abcdefghijklmnopqrstuvwxzy"[choice]
	return name
		

var player_info = {"name": rand_name(), "rtc_id": rtc.generate_unique_id()}
var players = {}
var rtc_to_enet = {}
var enet_to_rtc = {}



func join_game(address = ""):
	if address.is_empty():
		address = DEFAULT_SERVER_IP
	var peer = ENetMultiplayerPeer.new()
	var error = peer.create_client(address, PORT)
	print(address, PORT, error)
	if error:
		return error
	multiplayer.multiplayer_peer = peer
	rpc_state = "signaling"


# When the server decides to start the game from a UI scene,
# do Lobby.load_game.rpc(filepath)
@rpc("any_peer", "reliable")
func load_game(game_scene_path):
	get_tree().change_scene_to_file(game_scene_path)


# When a peer connects, send them my player info.
# This allows transfer of all desired data for each player, not only the unique ID.
func _on_player_connected(id):
	var me = player_info.get("name")
	var them = players.get(id, {}).get("name", null)
	print(me, " sends register to ", them)
	register_player.rpc_id(id, player_info)

func new_rtc_peer(id):
	var me = player_info.get("name")
	var them = players.get(id, {}).get("name", null)
	if id != 1: # ignore the signaling server!!
		var peer = WebRTCPeerConnection.new()
		#print("RTC id: ", rtc.get_unique_id(), " ENet id ", id, " Player Info RTC ID ", )
		#var rtc_id = rtc.get_unique_id()
		peer.initialize({
			"iceServers": [ { "urls": [STUN_SERVER] } ]
		})
		# TODO description, candidate
		peer.session_description_created.connect(_on_description_created.bind(id))
		peer.ice_candidate_created.connect(_on_candidate_created.bind(id))
		peer.data_channel_received.connect(_on_channel_created)
		channel = peer.create_data_channel("chat", {"negotiated": true, "id": 1})
		print(me, " connects new peer: ", them)
		#print(rtc.network_mode)
		rtc.add_peer(peer, id)
		
		# offer
		if id < rtc.get_unique_id():
			print(me, " makes RTC offer")
			peer.create_offer()
			
func _on_channel_created():
	print("data channel created")
	# close ENet connection to signaling server
	_disconnect_signal_server()
	#multiplayer.multiplayer_peer.close()
	# make new RPC calls use RTC
	multiplayer.multiplayer_peer = rtc
	rpc_state = "RTC"

		
func _on_description_created(type, sdp, recipient):
	var them = players.get(recipient, {}).get("name", null)
	rtc.get_peer(recipient).connection.set_local_description(type, sdp)
	
	#print(players, id)
	print(them, " sends description of type ", type)
	send_description.rpc_id(1, type, sdp, rtc_to_enet[recipient], rtc.get_unique_id())
	
@rpc("any_peer", "reliable")
func send_description(type, sdp, recipient, sender):
	pass

@rpc("any_peer", "reliable")
func receive_description(type, sdp, sender):
	print("got remote description")
	rtc.get_peer(sender).connection.set_remote_description(type, sdp)
	
func _on_candidate_created(media, index, name, recipient):
	send_candidate.rpc_id(1, media, index, name, rtc_to_enet[recipient], rtc.get_unique_id())
	
@rpc("any_peer", "reliable")
func send_candidate(media, index, name, sender, _recipient):
	print(sender, " created ice candidate for ", _recipient)
	rtc.get_peer(sender).connection.add_ice_candidate(media, index, name)

@rpc("any_peer", "reliable")
func register_player(new_player_info):
	print(multiplayer.get_remote_sender_id(), " sent register to ", multiplayer.get_unique_id())
	players[multiplayer.get_remote_sender_id()] = new_player_info

	rtc_to_enet[new_player_info["rtc_id"]] = multiplayer.get_remote_sender_id()
	enet_to_rtc[multiplayer.get_remote_sender_id()] = new_player_info["rtc_id"]
	
	new_rtc_peer(new_player_info["rtc_id"])

func _on_player_disconnected(id):
	players.erase(id)


func _on_connected_ok():
	var peer_id = multiplayer.get_unique_id()
	players[peer_id] = player_info
	rtc.create_mesh(player_info["rtc_id"])



func _on_connected_fail():
	multiplayer.multiplayer_peer = null


func _on_server_disconnected():
	print("server disconnected")
	multiplayer.multiplayer_peer = null
	players.clear()

func _input(event):
	if event is InputEventMouseButton:
		
		#print("My name is ", player_info["name"])
		#print("The players are: ", players)
		
		#for id in rtc.get_peers():
		#	var peer = rtc.get_peer(id)
		#	print("peer: ", peer)
		#	print("connection: ", peer.connection.get_connection_state())
		#	print("gathering: ", peer.connection.get_gathering_state())
		#	print("signaling: ", peer.connection.get_signaling_state())
		for peer in multiplayer.multiplayer_peer.get_peers().values():
			print("peer: ", peer)
			for channel in peer.get("channels"):
				print(channel.get_label(), " ", channel.get_ready_state())
			#print("connection: ", peer.get_connection_state())
			#print("gathering: ", peer.get_gathering_state())
			#print("signaling: ", peer.get_signaling_state())
		
		#send_message("Hello from " + player_info["name"])
		#print(multiplayer.multiplayer_peer.get_peers(), " ", rpc_state)
		
		#send_msg.rpc("RPC Hello from " + player_info["name"])

func _process(_delta):
	for peer in rtc.get_peers().values():
		#print(peer)
		var res = peer["connection"].poll()
		if res != 0:
			print("Polling error: ", res)
		
	if channel != null and channel.get_ready_state() == WebRTCDataChannel.STATE_OPEN:
		if rpc_state == "signaling":
			_on_channel_created()
		#while channel.get_available_packet_count() > 0:
			#print(String(get_path()), " received: ", channel.get_packet().get_string_from_utf8())
			
func ready():
	return channel != null and channel.get_ready_state() == WebRTCDataChannel.STATE_OPEN
