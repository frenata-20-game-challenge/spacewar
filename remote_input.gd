extends InputController

@export var thrust_act: String = "thrust"
@export var shoot_act: String = "shoot"
@export var left_act: String = "left"
@export var right_act: String = "right"


var player

var state = {
			thrust_act: false,
			left_act: false,
			right_act: false,
			shoot_act: false
			}

func set_pressed(act):
	state[act] = true

func is_action_pressed(act):
	var pressed = state[act]
	state[act] = false
	return pressed

func is_action_just_pressed(act):
	var pressed = state[act]
	state[act] = false
	return pressed
