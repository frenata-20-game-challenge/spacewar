extends Node2D

@export var enabled: bool = false
var direction = Vector2(randi_range(0, 600), randi_range(0,600))
var reload_rate = 50
var reload = 0

func _process(delta):
	if not enabled:
		return
		
	if reload > 0:
		reload -= 1
	
	if randi() % 4 == 0 and reload == 0:
		Input.action_press("blue-shoot")
		reload = reload_rate
		
	if abs(get_angle_to(direction)) < .5:
		if get_angle_to(direction) < 0:
			Input.action_press("blue-left")
		else:
			Input.action_press("blue-right")
	elif %Red != null and abs(get_angle_to(%Red.position)) < .25 and reload == 0:
		Input.action_press("blue-shoot")
		reload = reload_rate
	else:
		Input.action_press("blue-thrust")


func _on_eval_behavior():
	#print("change heading")
	direction = Vector2(randi_range(100, 500), randi_range(100,500))
