extends RigidBody2D

signal death(player)

@export var turning_rate: float = 200
@export var thrust: float = 400
@export var player: String
@export var input: InputController

var thrust_act: String = "thrust"
var shoot_act: String = "shoot"
var left_act: String = "left"
var right_act: String = "right"

var torpedo = preload("res://torpedo.tscn")
var translation = Vector2.ZERO

func set_translation(translation):
	self.translation = translation

func _integrate_forces(state):
	if translation != Vector2.ZERO:
		state.transform = Transform2D(transform.get_rotation(), transform.get_origin() + translation)
		translation = Vector2.ZERO
	
	var direction = 0
	if input.is_action_pressed(left_act):
		direction = -1
	elif input.is_action_pressed(right_act):
		direction = 1
	state.apply_torque(direction * turning_rate)
	
	if input.is_action_pressed(thrust_act):
		state.apply_force(Vector2(0, -thrust).rotated(rotation))
		%Emissions.emitting = true
	else:
		%Emissions.emitting = false
		
	if input.is_action_just_pressed(shoot_act):
		var shot = torpedo.instantiate()
		shot.new(Vector2(0, -thrust).rotated(rotation), position, rotation)
		shot.collision_mask = collision_mask
		shot.collision_layer = collision_layer
		shot.modulate = modulate
		get_parent().call_deferred("add_child", shot)
		%Sound.play()

func explode():
	death.emit(player)
	queue_free()


func _on_collision(body):
	if body.has_method("explode"):
		#print(self, " hit another ship")
		body.explode()
		explode()
	elif body.is_in_group("planetary"):
		#print(self, " hit a planet")
		explode()
